#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h" // Required for using single-precision variables.
#include "pico/double.h" // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.


/* FUNCTION PROTOTYPES */
void core1_entry(void); 
//void core2_entry(void);
float sin_approx_pi(int num_iterations);
double dbl_approx_pi(int num_iterations);

// Main code entry point for core0.
int main() {

 const int ITER_MAX = 100000;
 stdio_init_all();
 multicore_launch_core1(core1_entry);
 //multicore_launch_core1(core2_entry);


/* SEQUENTIAL RUN */

    /* total sequential timer variable declaration */
    uint32_t seq_start_time;
    uint32_t seq_end_time;
    uint32_t seq_runtime;

    /* single precision timer variable declaration */
    uint32_t sin_start_time;
    uint32_t sin_end_time;
    uint32_t sin_runtime;

    /* double precision timer variable declaration */
    uint32_t dbl_start_time;
    uint32_t dbl_end_time;
    uint32_t dbl_runtime;

    seq_start_time = time_us_32(); //start sequential timer

    /* Run the single-precision Wallis approximation */
    sin_start_time = time_us_32(); //start timer
    sin_approx_pi(ITER_MAX); //run wallis approximation function
    sin_end_time = time_us_32(); //end timer

    /* Run the double-precision Wallis approximation */
    dbl_start_time = time_us_32(); //start timer
    dbl_approx_pi(ITER_MAX); //run wallis approximation function
    dbl_end_time = time_us_32(); //end timer

    seq_end_time = time_us_32(); //end sequential timer

    /* measured runtime (sequential) in microseconds */
    sin_runtime = sin_end_time - sin_start_time;
    dbl_runtime = dbl_end_time - dbl_start_time;
    seq_runtime = seq_end_time - seq_start_time;

    /* Display time taken for application to run in sequential mode */
    printf("\n\nsingle-precision sequential runtime   = %u microseconds", sin_runtime);
    printf("\ndouble-precision sequential runtime   = %u microseconds", dbl_runtime);
    printf("\ntotal sequential runtime              = %u microseconds", seq_runtime);


/* PARALLEL RUN */

    /* parallel timer variable declaration */
    uint32_t parallel_start_time;
    uint32_t parallel_end_time;
    uint32_t parallel_runtime;

    parallel_start_time = time_us_32(); //start multicore timer

    /* Run the double-precision Wallis approximation on second core */
    dbl_start_time = time_us_32();
    multicore_fifo_push_blocking((uintptr_t) &dbl_approx_pi); //push double approx function to second core
    multicore_fifo_push_blocking(ITER_MAX); //push its argument to second core aswell

    /* Run the single-precision Wallis approximation on first core */
    sin_start_time = time_us_32(); //start timer
    sin_approx_pi(ITER_MAX); //run wallis approximation function
    sin_end_time = time_us_32(); //end timer

    /* return dbl_approx_pi result from second core */
    multicore_fifo_pop_blocking();
    dbl_end_time = time_us_32(); //end timer
    
    parallel_end_time = time_us_32(); //end multicore timer

    /* Take snapshot of timer and store */
    sin_runtime = sin_end_time - sin_start_time;
    dbl_runtime = dbl_end_time - dbl_start_time;
    parallel_runtime = parallel_end_time - parallel_start_time;

    // Display time taken for application to run in parallel mode
    printf("\n\nsingle-precision first core runtime   = %u microseconds", sin_runtime);
    printf("\ndouble-precision second core runtime  = %u microseconds", dbl_runtime);
    printf("\ntotal parallel runtime                = %u microseconds", parallel_runtime);

 return 0;
}


/**************************************************************************************************/
/* FUNCTIONS */
/**************************************************************************************************/

float sin_approx_pi(int num_iterations){

    float approx = 1, multiplier;

    for (float n=1.0; n<=num_iterations; n = n+1){
        multiplier = (4*n*n)/((4*n*n)-1);
        approx = approx*multiplier; //approximation assigned old approximation x newest iteration
    }

    approx = approx*2; //sum of products so far have been = pi/2
    return approx;
}

double dbl_approx_pi(int num_iterations){

    double approx = 1.0, multiplier;    

    for (double n=1.0; n<=num_iterations; n = n+1.0){
        multiplier = (4*n*n)/((4*n*n)-1);
        approx = approx*multiplier; //approximation assigned old approximation x newest iteration
    }
    approx = approx*2; //sum of products so far have been = pi/2
    return approx;
}




/**
* @brief This function acts as the main entry-point for core #1.
* A function pointer is passed in via the FIFO with one
* incoming int32_t used as a parameter. The function will
* provide an int32_t return value by pushing it back on
* the FIFO, which also indicates that the result is ready.
*/
void core1_entry() {
 while (1) {
 //
 int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
 int32_t p = multicore_fifo_pop_blocking();
 int32_t result = (*func)(p);
 multicore_fifo_push_blocking(result);
 }
}