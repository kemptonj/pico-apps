#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h" // Required for using single-precision variables.
#include "pico/double.h" // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
#include "hardware/regs/xip.h" //access to xip registers


/* FUNCTION PROTOTYPES */
void core1_entry(void); 
void sequential_run(void);
void parallel_run(void);
float sin_approx_pi(int num_iterations);
double dbl_approx_pi(int num_iterations);
//bool get_xip_cache_en(); // Function to get the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en); // Function to set the enable status of the XIP cache


// Main code entry point for core0.
int main() {

 stdio_init_all();
 multicore_launch_core1(core1_entry);
 
/*
measure performance...
1. Using a single CPU core with caches enabled
2. Using a single CPU core with caches disabled
3. Using both CPU cores with caches enabled
4. Using both CPU cores with caches disabled
*/

/* Using a single CPU core with caches enabled */
set_xip_cache_en(true);
printf("-----------------------\ncache enabled:\n");
sequential_run();

/* Using a single CPU core with caches disabled */
set_xip_cache_en(false);
printf("-----------------------\ncache disabled:\n");
sequential_run();

/* Using both CPU cores with caches enabled */
set_xip_cache_en(true);
printf("-----------------------\ncache enabled:\n");
parallel_run();

/* Using both CPU cores with caches disabled */
set_xip_cache_en(false);
printf("-----------------------\ncache disabled:\n");
parallel_run();

 return 0;
}


/**************************************************************************************************/
/* FUNCTIONS */
/**************************************************************************************************/

float sin_approx_pi(int num_iterations){

    float approx = 1, multiplier;

    for (float n=1.0; n<=num_iterations; n = n+1){
        multiplier = (4*n*n)/((4*n*n)-1);
        approx = approx*multiplier; //approximation assigned old approximation x newest iteration
    }

    approx = approx*2; //sum of products so far have been = pi/2
    return approx;
}

double dbl_approx_pi(int num_iterations){

    double approx = 1.0, multiplier;    

    for (double n=1.0; n<=num_iterations; n = n+1.0){
        multiplier = (4*n*n)/((4*n*n)-1);
        approx = approx*multiplier; //approximation assigned old approximation x newest iteration
    }
    approx = approx*2; //sum of products so far have been = pi/2
    return approx;
}


/**
* @brief This function acts as the main entry-point for core #1.
* A function pointer is passed in via the FIFO with one
* incoming int32_t used as a parameter. The function will
* provide an int32_t return value by pushing it back on
* the FIFO, which also indicates that the result is ready.
*/
void core1_entry() {
 while (1) {
 //
 int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
 int32_t p = multicore_fifo_pop_blocking();
 int32_t result = (*func)(p);
 multicore_fifo_push_blocking(result);
 }
}


void sequential_run(void){

const int ITER_MAX = 100000;

/* SEQUENTIAL RUN */

    /* total sequential timer variable declaration */
    uint32_t seq_start_time;
    uint32_t seq_end_time;
    uint32_t seq_runtime;

    /* single precision timer variable declaration */
    uint32_t sin_start_time;
    uint32_t sin_end_time;
    uint32_t sin_runtime;

    /* double precision timer variable declaration */
    uint32_t dbl_start_time;
    uint32_t dbl_end_time;
    uint32_t dbl_runtime;

    seq_start_time = time_us_32(); //start sequential timer

    /* Run the single-precision Wallis approximation */
    sin_start_time = time_us_32(); //start timer
    sin_approx_pi(ITER_MAX); //run wallis approximation function
    sin_end_time = time_us_32(); //end timer

    /* Run the double-precision Wallis approximation */
    dbl_start_time = time_us_32(); //start timer
    dbl_approx_pi(ITER_MAX); //run wallis approximation function
    dbl_end_time = time_us_32(); //end timer

    seq_end_time = time_us_32(); //end sequential timer

    /* measured runtime (sequential) in microseconds */
    sin_runtime = sin_end_time - sin_start_time;
    dbl_runtime = dbl_end_time - dbl_start_time;
    seq_runtime = seq_end_time - seq_start_time;

    /* Display time taken for application to run in sequential mode */
    printf("single-precision sequential runtime   = %u microseconds\n", sin_runtime);
    printf("double-precision sequential runtime   = %u microseconds\n", dbl_runtime);
    printf("total sequential runtime              = %u microseconds\n", seq_runtime);

}


void parallel_run(void) {

const int ITER_MAX = 100000;

/* PARALLEL RUN */

    /* single precision timer variable declaration */
    uint32_t sin_start_time;
    uint32_t sin_end_time;
    uint32_t sin_runtime;

    /* double precision timer variable declaration */
    uint32_t dbl_start_time;
    uint32_t dbl_end_time;
    uint32_t dbl_runtime;

    /* parallel timer variable declaration */
    uint32_t parallel_start_time;
    uint32_t parallel_end_time;
    uint32_t parallel_runtime;

    parallel_start_time = time_us_32(); //start multicore timer

    /* Run the double-precision Wallis approximation on second core */
    dbl_start_time = time_us_32();
    multicore_fifo_push_blocking((uintptr_t) &dbl_approx_pi); //push double approx function to second core
    multicore_fifo_push_blocking(ITER_MAX); //push its argument to second core aswell

    /* Run the single-precision Wallis approximation on first core */
    sin_start_time = time_us_32(); //start timer
    sin_approx_pi(ITER_MAX); //run wallis approximation function
    sin_end_time = time_us_32(); //end timer

    /* return dbl_approx_pi result from second core */
    multicore_fifo_pop_blocking();
    dbl_end_time = time_us_32(); //end timer
    
    parallel_end_time = time_us_32(); //end multicore timer

    /* Take snapshot of timer and store */
    sin_runtime = sin_end_time - sin_start_time;
    dbl_runtime = dbl_end_time - dbl_start_time;
    parallel_runtime = parallel_end_time - parallel_start_time;

    // Display time taken for application to run in parallel mode
    printf("single-precision first core runtime   = %u microseconds\n", sin_runtime);
    printf("double-precision second core runtime  = %u microseconds\n", dbl_runtime);
    printf("total parallel runtime                = %u microseconds\n", parallel_runtime);

}


// Function to get the enable status of the XIP cache
/* get the correct bit of the register that controls the functionality of the XIP cache */
/* pico SDK pg 73: ro = read only access*/
// bool get_xip_cache_en(){

// io_ro_32

//     return 0;
// }


/* set the correct bit of the register that controls the functionality of the XIP cache */
/*
FUNCTION ORIGINS

    pico SDK pg 74:

        "static __force_inline void hw_clear_bits (io_rw_32 *addr, uint32_t mask)
        Atomically clear the specified bits to 0 in a HW register."

        "static __force_inline void hw_set_bits (io_rw_32 *addr,uint32_t mask)
        Atomically set the specified bits to 1 in a HW register."

    RP datasheet pg153: POWER DOWN: "Writing 1 to this bit forces CTRL_EN to 0, i.e. the cache cannot"
    pg 154: EN: "When 1, enable the cache"
*/
bool set_xip_cache_en(bool cache_en){

    

    /* set to enabled */ 
    if (cache_en){

        /* force 'power down' bits to 0, so cache can actually be written to */
        hw_clear_bits((io_rw_32 *)XIP_CTRL_BASE, XIP_CTRL_POWER_DOWN_BITS); //hw_clear = set to 0

        /* write 1 to 'enable' bits, thus enabling cache*/
        hw_set_bits((io_rw_32 *)XIP_CTRL_BASE,  XIP_CTRL_EN_BITS); //hw_set = set to 1
    }

    else {

        /* write 0 to 'enable' bits, thus disabling cache*/
        hw_clear_bits((io_rw_32 *)XIP_CTRL_BASE,  XIP_CTRL_EN_BITS); //hw_clear = set to 0

        /* force 'power down' bits to 1, to turn off cache entirely, it can't be written to */
        hw_set_bits((io_rw_32 *)XIP_CTRL_BASE, XIP_CTRL_POWER_DOWN_BITS); //hw_set = set to 1

    }


    return cache_en;
}