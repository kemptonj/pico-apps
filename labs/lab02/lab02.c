#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     //uncomment when using wokwi
#include "pico/double.h"    //uncomment when using wokwi

//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

//function prototypes
float sin_approx_pi(void);
double dbl_approx_pi(void);
void print_sin_errors(float);
void print_dbl_errors(double);


int main() {

    stdio_init_all();
    printf("\npi (single-precision)  = %f\n", sin_approx_pi());
    print_sin_errors(sin_approx_pi());
    
    printf("\npi (double-precision)  = %f\n", dbl_approx_pi());
    print_dbl_errors(dbl_approx_pi());

    return 0;
}
//////////////////////////////////////////////////////////////////////////////////////handles floats/////////
/**
 * @brief approximate pi to float precision using wallis product 
 * 
 * @return float 
 */
float sin_approx_pi(void){

    float approx = 1, multiplier;

    for (float n=1.0; n<=100000; n = n+1){
        multiplier = (4*n*n)/((4*n*n)-1);
        approx = approx*multiplier; //approximation assigned old approximation x newest iteration
    }

    approx = approx*2; //sum of products so far have been = pi/2
    return approx;
}

/**
 * @brief calculate and print error of single-precision approximation of pi
 * 
 * abs error = (actual - expected)
 * rel error = (actual - expected)/expected
 * % error = rel error * 100
 * 
 * @param sin_approx 
 * @return float 
 */
void print_sin_errors(float sin_approx){
    
    float approx_abs_error = (sin_approx - 3.14159265359);
    printf("approx. absolute error = %f\n", approx_abs_error);

    float approx_rel_error = (sin_approx - 3.14159265359)/3.14159265359;
    printf("approx. relative error = %f\n", approx_rel_error);
    printf("approx. percent. error = %f\n", approx_rel_error*100);

}

/////////////////////////////////////////////////////////////////////////////////////handles doubles//////////
/**
 * @brief approximate pi to double precision using wallis product 
 * 
 * @return double 
 */
double dbl_approx_pi(void){

    double approx = 1.0, multiplier;    

    for (double n=1.0; n<=100000; n = n+1.0){
        multiplier = (4*n*n)/((4*n*n)-1);
        approx = approx*multiplier; //approximation assigned old approximation x newest iteration
    }
    approx = approx*2; //sum of products so far have been = pi/2
    return approx;
}

/**
 * @brief calculate and print error of double-precision approximation of pi
 * 
 * abs error = (actual - expected)
 * rel error = (actual - expected)/expected
 * % error = rel error * 100
 * 
 * @param dbl_approx 
 * @return double 
 */
void print_dbl_errors(double dbl_approx){
    
    double approx_abs_error = (dbl_approx - 3.14159265359);
    printf("approx. absolute error = %f\n", approx_abs_error);
    
    double approx_rel_error = (dbl_approx - 3.14159265359)/3.14159265359;
    printf("approx. relative error = %f\n", approx_rel_error);
    printf("approx. percent. error = %f\n", approx_rel_error*100);

}